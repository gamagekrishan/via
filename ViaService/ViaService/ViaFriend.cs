﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViaService
{
   public class ViaFriend 
    {
        private int _requester;
        private int _accepter;
        private DateTime _date;
        private int _status;
        private int _id;
        private string _fName;
        private string _lName;
        private string _email;
        private byte[] _image;

        public int ID
        {
            get
            {
                return this._id;
            }
            set
            {
                this._id = value;
            }
        }

        public string FirstName
        {
            get { return this._fName; }
            set { this._fName = value; }
        }

        public string LastName
        {
            get { return this._lName; }
            set { this._lName = value; }
        }

        public string Emial
        {
            get
            {
                return this._email;
            }
            set
            {
                this._email = value;
            }
        }

        public byte[] Image
        {
            get
            {
                return this._image;
            }
            set
            {
                this._image = value;
            }
        }

        public int Requester
        {
            get
            {
                return this._requester;
            }
            set
            {
                this._requester = value;
            }
        }

        public int Accepter
        {
            get
            {
                return this._accepter;
            }
            set
            {
                this._accepter = value;
            }
        }

        public DateTime Date
        {
            get
            {
                return this._date;
            }
            set
            {
                this._date = value;
            }
        }

        public int Status
        {
            get
            {
                return this._status;
            }
            set
            {
                this._status = value;
            }
        }

        public ViaFriend() { }

        public ViaFriend(int id,string fName,string lName,string email,byte[] image,int requester,int accepter,DateTime date,int status)
        {
            this._id = id;
            this._fName = fName;
            this._lName = lName;
            this._email = email;
            this.Image = image;
            this._requester = requester;
            this._accepter = accepter;
            this._date = date;
            this._status = status;
        }
    }
}
