﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ViaService
{
     public static class ViaConnector
    {
        private static Dictionary<int, IViaServiceCallBack> _usersList = new Dictionary<int, IViaServiceCallBack>();
        private static Dictionary<int, LoginStatus> _usersStatusList = new Dictionary<int, LoginStatus>();
        private static object usersListLock = new object();
        private static object usersStatusListLock = new object();
        private static object userDialLock = new object();
        private static object userHoldLock = new object();
        private static object userCutLock = new object();
        private static ReaderWriterLockSlim userVideoLock = new ReaderWriterLockSlim();
        //private ReaderWriterLockSlim subscribersLock = new ReaderWriterLockSlim();

        public static void AddUser(int userId, IViaServiceCallBack callBack)
        {
            lock (usersListLock)
            {
                if (_usersList.ContainsKey(userId))
                {
                    _usersList[userId] = callBack;
                }
                else
                {
                    _usersList.Add(userId, callBack);
                }
            }
        }

        public static Dictionary<int, LoginStatus> CheckLoginStatus(List<ViaFriend> fList)
        {
            lock (usersStatusListLock)
            {
                Dictionary<int, LoginStatus> statusList = new Dictionary<int, LoginStatus>();
                foreach (ViaFriend friend in fList)
                {
                    if (_usersStatusList.ContainsKey(friend.ID))
                    {
                        statusList.Add(friend.ID, _usersStatusList[friend.ID]);
                    }
                    else
                    {
                        statusList.Add(friend.ID, LoginStatus.Offline);
                    }
                }
                return statusList;
            }
        }

        //public bool SendMessage(ViaMessage msg)
        //{
        //    lock (_usersList)
        //    {

        //    }
        //}


        public static void SetUserStatus(int userId, LoginStatus status)
        {
            lock (usersStatusListLock)
            {
                if (_usersStatusList.ContainsKey(userId))
                {
                    _usersStatusList[userId] = status;
                }
                else
                {
                    _usersStatusList.Add(userId, status);
                }
            }
        }

         public static LoginStatus GetStatusById(int userId)
         {
             lock (usersStatusListLock)
             {
                 if (_usersStatusList.ContainsKey(userId))
                 {
                     return _usersStatusList[userId];
                 }
                 else
                 {
                     return LoginStatus.Offline;
                 }
             }
         }
         public static bool SendStatus(int to,int from, LoginStatus status)
         {
             try
             {
                 lock (usersListLock)
                 {
                     if (_usersList.ContainsKey(to))
                     {
                         _usersList[to].SyncFriendsStatus(from, status);
                         return true;
                     }
                     else
                     {
                         return true;
                     }
                 }

             }
             catch (Exception)
             {
                 return false;
             }
         }

         public static MessageArgs SendMsg(ViaMessage msg)
         {
             try
             {
                 lock (usersListLock)
                 {
                     if (_usersList.ContainsKey(msg.Receiver))
                     {
                         MessageArgs result = _usersList[msg.Receiver].MessageReceived(msg);
                         if (result.Equals(MessageArgs.Received))
                         {
                             return MessageArgs.Send;
                         }
                         else
                         {
                             return MessageArgs.SendingFail;
                         }
                     }
                     else
                     {
                         return MessageArgs.SendingFail;
                     }
                 }
             }
             catch (Exception)
             {
                 return MessageArgs.SendingFail;
             }
         }

         public static void SendContact(ViaFriend contact, int to)
         {
             try
             {
                 lock (usersListLock)
                 {
                     if (_usersList.ContainsKey(to))
                     {
                         List<ViaFriend> fList = new List<ViaFriend>();
                         fList.Add(contact);
                         _usersList[to].SyncContacts(fList);
                     }
                 }
             }
             catch (Exception)
             {
                 //Console.WriteLine("Contact Send Fail");
             }
         }

         public static void RemoveCallBack(int userId)
         {
             lock (usersListLock)
             {
                 if (_usersList.ContainsKey(userId))
                 {
                     _usersList.Remove(userId);
                 }
             }
         }

         public static void RemoveStatus(int userId)
         {
             lock (usersStatusListLock)
             {
                 if (_usersStatusList.ContainsKey(userId))
                 {
                     _usersStatusList.Remove(userId);
                 }
             }
         }

         public static IViaServiceCallBack getVideoCall(int id)
         {
             IViaServiceCallBack user = null;
             lock (userDialLock)
             {
                 if (_usersList.ContainsKey(id))
                 {
                     user = _usersList[id];
                 }
                 return user;
             }
             //try
             //{
             //    //lock (subscribersLock)
             //    //{
             //    //subscribersLock.EnterReadLock();
             //    IViaServiceCallBack user = _usersList[id];
             //    if (callType == callType.call)
             //    {
             //        user.OnDial(id, callType);
             //    }
             //    else if (callType == callType.cut)
             //    {
             //        user.OnDial(id, callType);
             //    }
             //    else if (callType == callType.incomeAnswer)
             //    {
             //        user.OnVideo(id, callType);
             //    }
             //    else if (callType == callType.incomeCut)
             //    {
             //        user.OnVideo(id, callType);
             //    }
             //    else
             //    {
             //        Console.WriteLine("Error in Client Application");
             //    }
             //    //}
             //}
             //finally
             //{
             //    //subscribersLock.ExitReadLock();
             //}

         }

        public static IViaServiceCallBack holdVideoCall(int id)
        {
            IViaServiceCallBack user = null;
            lock (userHoldLock)
            {
                if (_usersList.ContainsKey(id))
                {
                    user = _usersList[id];
                }
                return user;
            }
        }

        public static IViaServiceCallBack CutCall(int id)
        {
            IViaServiceCallBack user = null;
            lock(userCutLock)
            {
                if(_usersList.ContainsKey(id))
                {
                    user = _usersList[id];
                }
                return user;
            }
        }

         public static IViaServiceCallBack publishVideoCall(int id)
         {
             IViaServiceCallBack user = null;
             lock (userVideoLock)
             {
                 if (_usersList.ContainsKey(id))
                 {
                     user = _usersList[id];
                 }
                 return user;
             }
             //try
             //{
             //    //lock (subscribersLock)
             //    //{
             //    //subscribersLock.EnterReadLock();
             //    IViaServiceCallBack user = _usersList[id];
             //    user.OnVideoSend(id, data);
             //    //}
             //}
             //finally
             //{
             //    //subscribersLock.ExitReadLock();
             //}
         }
    }
}
