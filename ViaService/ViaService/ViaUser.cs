﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace ViaService
{
    
    public class ViaUser
    {
        private int _id;
        private string _fName;
        private string _lName;
        private string _email;
        private byte[] _image;

        public int ID
        {
            get
            {
                return this._id;
            }
            set
            {
                this._id = value;
            }
        }

        public string FirstName
        {
            get { return this._fName; }
            set { this._fName = value; }
        }

        public string LastName
        {
            get { return this._lName; }
            set { this._lName = value; }
        }

        public string Emial
        {
            get
            {
                return this._email;
            }
            set
            {
                this._email = value;
            }
        }

        public byte[] Image
        {
            get
            {
                return this._image;
            }
            set
            {
                this._image = value;
            }
        }

        public ViaUser()
        {

        }

        
        public ViaUser(int id,string fName,string lName,string email,byte[] image)
        {
            this._id = id;
            this._fName = fName;
            this._lName = lName;
            this._email = email;
            this.Image = image;
        }
    }
}
