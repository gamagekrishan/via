﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace ViaConsoleHost
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ServiceHost host = new ServiceHost(typeof(ViaService.ViaService))) {
                using (ServiceHost host1 = new ServiceHost(typeof(ViaService.ViaWebService)))
                {
                    host.Open();
                    Console.WriteLine("ViaService started at: "+DateTime.Now);

                    host1.Open();
                    Console.WriteLine("ViaWebService started at: "+DateTime.Now);

                    Console.ReadLine();
                }
            }
        }
    }
}
