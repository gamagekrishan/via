﻿namespace ChatApplication
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.panel1 = new System.Windows.Forms.Panel();
            this.flopnlSearch = new System.Windows.Forms.FlowLayoutPanel();
            this.flopnlRecent = new System.Windows.Forms.FlowLayoutPanel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.floFriendList = new System.Windows.Forms.FlowLayoutPanel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pnlSearch = new System.Windows.Forms.Panel();
            this.pnlContact = new System.Windows.Forms.Panel();
            this.pnlRecent = new System.Windows.Forms.Panel();
            this.pnlRecentCount = new System.Windows.Forms.Panel();
            this.lblRecentCount = new System.Windows.Forms.Label();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.ovlRecentCount = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblUserStatus = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.shapeContainer3 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.picBoxUserStatus = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.picBoxUser = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.pnlInitialContact = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pnlSearching = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pnlNoSearchResult = new System.Windows.Forms.Panel();
            this.lblNoSearchResult = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.pnlVideoCall = new System.Windows.Forms.Panel();
            this.pnlRing = new System.Windows.Forms.Panel();
            this.picCutOut = new System.Windows.Forms.PictureBox();
            this.picCutIn = new System.Windows.Forms.PictureBox();
            this.picAnswerIn = new System.Windows.Forms.PictureBox();
            this.lblCallStatus = new System.Windows.Forms.Label();
            this.pictureBoxGif = new System.Windows.Forms.PictureBox();
            this.pictureBoxVideoIn = new System.Windows.Forms.PictureBox();
            this.pictureBoxVideoOut = new System.Windows.Forms.PictureBox();
            this.picManualStart = new System.Windows.Forms.PictureBox();
            this.picStart = new System.Windows.Forms.PictureBox();
            this.picStop = new System.Windows.Forms.PictureBox();
            this.pnlMsgWait = new System.Windows.Forms.Panel();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pnlSearchMessageContainer = new System.Windows.Forms.Panel();
            this.pnlMsg = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.pnlVideoDial = new System.Windows.Forms.Panel();
            this.pnlConnecting = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.lblSelectUserStatus = new System.Windows.Forms.Label();
            this.lblSelectUserName = new System.Windows.Forms.Label();
            this.shapeContainer4 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.ovlSelectUserStatus = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.picSelectedUser = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.pnlType = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.txtMsgContent = new System.Windows.Forms.TextBox();
            this.pnlAddContactContainer = new System.Windows.Forms.Panel();
            this.txtHideSearchContactId = new System.Windows.Forms.TextBox();
            this.pnlAddContactMsg = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.pnlAddContactMsgCancel = new System.Windows.Forms.Panel();
            this.lblAddContactMsgCancelBtn = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.lblAddContactMsgSendButton = new System.Windows.Forms.Label();
            this.txtAddContactMsg = new System.Windows.Forms.TextBox();
            this.lblAddContactMsg = new System.Windows.Forms.Label();
            this.shapeContainer5 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.ovlAddContactMsgCancelBtnLeft = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.ovlAddContactMsgCancelBtnRight = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.ovlAddContactMsgRight = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.ovlAddContactMsgLeft = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.label6 = new System.Windows.Forms.Label();
            this.pnlAddcontact = new System.Windows.Forms.Panel();
            this.lblAddContactBtn = new System.Windows.Forms.Label();
            this.lblAddContact = new System.Windows.Forms.Label();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.ovlAddContactLeft = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.ovalAddContactRight = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.imgList = new System.Windows.Forms.ImageList(this.components);
            this.imgListStatus = new System.Windows.Forms.ImageList(this.components);
            this.timerCheckConnection = new System.Windows.Forms.Timer(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel4.SuspendLayout();
            this.pnlRecent.SuspendLayout();
            this.pnlRecentCount.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnlInitialContact.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.pnlSearching.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.pnlNoSearchResult.SuspendLayout();
            this.panel7.SuspendLayout();
            this.pnlVideoCall.SuspendLayout();
            this.pnlRing.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCutOut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCutIn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAnswerIn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGif)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxVideoIn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxVideoOut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picManualStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picStop)).BeginInit();
            this.pnlMsgWait.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.panel8.SuspendLayout();
            this.pnlConnecting.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.pnlType.SuspendLayout();
            this.panel11.SuspendLayout();
            this.pnlAddContactContainer.SuspendLayout();
            this.pnlAddContactMsg.SuspendLayout();
            this.panel9.SuspendLayout();
            this.pnlAddContactMsgCancel.SuspendLayout();
            this.panel13.SuspendLayout();
            this.pnlAddcontact.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.MintCream;
            this.panel1.Controls.Add(this.flopnlSearch);
            this.panel1.Controls.Add(this.flopnlRecent);
            this.panel1.Controls.Add(this.panel10);
            this.panel1.Controls.Add(this.floFriendList);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.pnlInitialContact);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(849, 538);
            this.panel1.TabIndex = 1;
            // 
            // flopnlSearch
            // 
            this.flopnlSearch.AutoScroll = true;
            this.flopnlSearch.Location = new System.Drawing.Point(1, 178);
            this.flopnlSearch.Name = "flopnlSearch";
            this.flopnlSearch.Size = new System.Drawing.Size(250, 379);
            this.flopnlSearch.TabIndex = 4;
            // 
            // flopnlRecent
            // 
            this.flopnlRecent.AutoScroll = true;
            this.flopnlRecent.Location = new System.Drawing.Point(1, 178);
            this.flopnlRecent.Name = "flopnlRecent";
            this.flopnlRecent.Size = new System.Drawing.Size(250, 379);
            this.flopnlRecent.TabIndex = 4;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.MintCream;
            this.panel10.Controls.Add(this.txtSearch);
            this.panel10.Controls.Add(this.pictureBox1);
            this.panel10.Controls.Add(this.label1);
            this.panel10.Location = new System.Drawing.Point(0, 134);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(250, 42);
            this.panel10.TabIndex = 2;
            // 
            // txtSearch
            // 
            this.txtSearch.BackColor = System.Drawing.Color.MintCream;
            this.txtSearch.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSearch.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearch.Location = new System.Drawing.Point(36, 11);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(199, 16);
            this.txtSearch.TabIndex = 80;
            this.txtSearch.TabStop = false;
            this.txtSearch.Text = "Search";
            this.txtSearch.Enter += new System.EventHandler(this.textBox1_Enter);
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            this.txtSearch.Leave += new System.EventHandler(this.textBox1_Leave);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::ChatApplication.Properties.Resources.tool;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(12, 8);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(17, 19);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.label1.Location = new System.Drawing.Point(17, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(218, 1);
            this.label1.TabIndex = 0;
            // 
            // floFriendList
            // 
            this.floFriendList.AutoScroll = true;
            this.floFriendList.Location = new System.Drawing.Point(0, 177);
            this.floFriendList.Name = "floFriendList";
            this.floFriendList.Size = new System.Drawing.Size(250, 379);
            this.floFriendList.TabIndex = 3;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.pnlSearch);
            this.panel4.Controls.Add(this.pnlContact);
            this.panel4.Controls.Add(this.pnlRecent);
            this.panel4.Location = new System.Drawing.Point(0, 91);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(250, 45);
            this.panel4.TabIndex = 2;
            // 
            // pnlSearch
            // 
            this.pnlSearch.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.pnlSearch.BackgroundImage = global::ChatApplication.Properties.Resources.people;
            this.pnlSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pnlSearch.Location = new System.Drawing.Point(166, 0);
            this.pnlSearch.Name = "pnlSearch";
            this.pnlSearch.Size = new System.Drawing.Size(83, 45);
            this.pnlSearch.TabIndex = 3;
            this.pnlSearch.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pnlSearch_MouseClick);
            this.pnlSearch.MouseEnter += new System.EventHandler(this.pnlSearch_MouseEnter);
            this.pnlSearch.MouseLeave += new System.EventHandler(this.pnlSearch_MouseLeave);
            // 
            // pnlContact
            // 
            this.pnlContact.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.pnlContact.BackgroundImage = global::ChatApplication.Properties.Resources.business;
            this.pnlContact.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pnlContact.Location = new System.Drawing.Point(83, 0);
            this.pnlContact.Name = "pnlContact";
            this.pnlContact.Size = new System.Drawing.Size(83, 45);
            this.pnlContact.TabIndex = 2;
            this.pnlContact.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pnlContact_MouseClick);
            this.pnlContact.MouseEnter += new System.EventHandler(this.pnlContact_MouseEnter);
            this.pnlContact.MouseLeave += new System.EventHandler(this.pnlContact_MouseLeave);
            // 
            // pnlRecent
            // 
            this.pnlRecent.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.pnlRecent.BackgroundImage = global::ChatApplication.Properties.Resources.exclamation__2_;
            this.pnlRecent.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pnlRecent.Controls.Add(this.pnlRecentCount);
            this.pnlRecent.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlRecent.Location = new System.Drawing.Point(0, 0);
            this.pnlRecent.Name = "pnlRecent";
            this.pnlRecent.Size = new System.Drawing.Size(83, 45);
            this.pnlRecent.TabIndex = 2;
            this.pnlRecent.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pnlRecent_MouseClick);
            this.pnlRecent.MouseEnter += new System.EventHandler(this.pnlRecent_MouseEnter_1);
            this.pnlRecent.MouseLeave += new System.EventHandler(this.pnlRecent_MouseLeave);
            // 
            // pnlRecentCount
            // 
            this.pnlRecentCount.BackColor = System.Drawing.Color.Transparent;
            this.pnlRecentCount.Controls.Add(this.lblRecentCount);
            this.pnlRecentCount.Controls.Add(this.shapeContainer2);
            this.pnlRecentCount.Location = new System.Drawing.Point(56, 0);
            this.pnlRecentCount.Name = "pnlRecentCount";
            this.pnlRecentCount.Size = new System.Drawing.Size(27, 27);
            this.pnlRecentCount.TabIndex = 2;
            // 
            // lblRecentCount
            // 
            this.lblRecentCount.AutoSize = true;
            this.lblRecentCount.BackColor = System.Drawing.Color.Orange;
            this.lblRecentCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRecentCount.Location = new System.Drawing.Point(7, 6);
            this.lblRecentCount.Name = "lblRecentCount";
            this.lblRecentCount.Size = new System.Drawing.Size(14, 13);
            this.lblRecentCount.TabIndex = 0;
            this.lblRecentCount.Text = "5";
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.ovlRecentCount});
            this.shapeContainer2.Size = new System.Drawing.Size(27, 27);
            this.shapeContainer2.TabIndex = 0;
            this.shapeContainer2.TabStop = false;
            // 
            // ovlRecentCount
            // 
            this.ovlRecentCount.BackColor = System.Drawing.Color.Orange;
            this.ovlRecentCount.BorderColor = System.Drawing.Color.DarkOrange;
            this.ovlRecentCount.BorderWidth = 2;
            this.ovlRecentCount.FillColor = System.Drawing.Color.Orange;
            this.ovlRecentCount.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.ovlRecentCount.Location = new System.Drawing.Point(4, 3);
            this.ovlRecentCount.Name = "ovlRecentCount";
            this.ovlRecentCount.Size = new System.Drawing.Size(18, 18);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.lblUserStatus);
            this.panel2.Controls.Add(this.lblUserName);
            this.panel2.Controls.Add(this.shapeContainer3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(849, 91);
            this.panel2.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.BackgroundImage = global::ChatApplication.Properties.Resources.logone;
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel3.Location = new System.Drawing.Point(206, 8);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(38, 39);
            this.panel3.TabIndex = 5;
            this.panel3.Click += new System.EventHandler(this.panel3_Click);
            // 
            // lblUserStatus
            // 
            this.lblUserStatus.AutoSize = true;
            this.lblUserStatus.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserStatus.ForeColor = System.Drawing.Color.White;
            this.lblUserStatus.Location = new System.Drawing.Point(84, 63);
            this.lblUserStatus.Name = "lblUserStatus";
            this.lblUserStatus.Size = new System.Drawing.Size(47, 15);
            this.lblUserStatus.TabIndex = 2;
            this.lblUserStatus.Text = "ONLINE";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.ForeColor = System.Drawing.Color.White;
            this.lblUserName.Location = new System.Drawing.Point(84, 38);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(118, 20);
            this.lblUserName.TabIndex = 1;
            this.lblUserName.Text = "Ishara Madawa";
            // 
            // shapeContainer3
            // 
            this.shapeContainer3.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer3.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer3.Name = "shapeContainer3";
            this.shapeContainer3.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.picBoxUserStatus,
            this.picBoxUser});
            this.shapeContainer3.Size = new System.Drawing.Size(849, 91);
            this.shapeContainer3.TabIndex = 4;
            this.shapeContainer3.TabStop = false;
            // 
            // picBoxUserStatus
            // 
            this.picBoxUserStatus.BackColor = System.Drawing.Color.Transparent;
            this.picBoxUserStatus.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("picBoxUserStatus.BackgroundImage")));
            this.picBoxUserStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.picBoxUserStatus.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.picBoxUserStatus.BorderWidth = 2;
            this.picBoxUserStatus.Cursor = System.Windows.Forms.Cursors.Default;
            this.picBoxUserStatus.Enabled = false;
            this.picBoxUserStatus.Location = new System.Drawing.Point(58, 61);
            this.picBoxUserStatus.Name = "picBoxUserStatus";
            this.picBoxUserStatus.SelectionColor = System.Drawing.Color.Transparent;
            this.picBoxUserStatus.Size = new System.Drawing.Size(17, 17);
            // 
            // picBoxUser
            // 
            this.picBoxUser.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("picBoxUser.BackgroundImage")));
            this.picBoxUser.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picBoxUser.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.picBoxUser.Location = new System.Drawing.Point(8, 8);
            this.picBoxUser.Name = "picBoxUser";
            this.picBoxUser.SelectionColor = System.Drawing.Color.Transparent;
            this.picBoxUser.Size = new System.Drawing.Size(70, 70);
            // 
            // pnlInitialContact
            // 
            this.pnlInitialContact.BackColor = System.Drawing.Color.MintCream;
            this.pnlInitialContact.Controls.Add(this.label4);
            this.pnlInitialContact.Controls.Add(this.pictureBox2);
            this.pnlInitialContact.Location = new System.Drawing.Point(32, 228);
            this.pnlInitialContact.Name = "pnlInitialContact";
            this.pnlInitialContact.Size = new System.Drawing.Size(200, 224);
            this.pnlInitialContact.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.label4.Location = new System.Drawing.Point(30, 165);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(140, 43);
            this.label4.TabIndex = 1;
            this.label4.Text = "Initializing Contacts,\r\nPlease Wait...";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(30, 14);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Padding = new System.Windows.Forms.Padding(5);
            this.pictureBox2.Size = new System.Drawing.Size(140, 138);
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // pnlSearching
            // 
            this.pnlSearching.Controls.Add(this.label3);
            this.pnlSearching.Controls.Add(this.pictureBox4);
            this.pnlSearching.Location = new System.Drawing.Point(3, 3);
            this.pnlSearching.Name = "pnlSearching";
            this.pnlSearching.Size = new System.Drawing.Size(246, 63);
            this.pnlSearching.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.DarkCyan;
            this.label3.Location = new System.Drawing.Point(65, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(176, 15);
            this.label3.TabIndex = 1;
            this.label3.Text = "Searching Via directory...";
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(4, 3);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Padding = new System.Windows.Forms.Padding(2);
            this.pictureBox4.Size = new System.Drawing.Size(55, 54);
            this.pictureBox4.TabIndex = 0;
            this.pictureBox4.TabStop = false;
            // 
            // pnlNoSearchResult
            // 
            this.pnlNoSearchResult.Controls.Add(this.lblNoSearchResult);
            this.pnlNoSearchResult.Location = new System.Drawing.Point(3, 3);
            this.pnlNoSearchResult.Name = "pnlNoSearchResult";
            this.pnlNoSearchResult.Size = new System.Drawing.Size(246, 155);
            this.pnlNoSearchResult.TabIndex = 0;
            // 
            // lblNoSearchResult
            // 
            this.lblNoSearchResult.AutoSize = true;
            this.lblNoSearchResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoSearchResult.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.lblNoSearchResult.Location = new System.Drawing.Point(52, 82);
            this.lblNoSearchResult.Name = "lblNoSearchResult";
            this.lblNoSearchResult.Size = new System.Drawing.Size(128, 16);
            this.lblNoSearchResult.TabIndex = 0;
            this.lblNoSearchResult.Text = "There is no result";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.White;
            this.panel7.Controls.Add(this.pnlVideoCall);
            this.panel7.Controls.Add(this.pnlMsgWait);
            this.panel7.Controls.Add(this.pnlSearchMessageContainer);
            this.panel7.Controls.Add(this.pnlMsg);
            this.panel7.Controls.Add(this.panel8);
            this.panel7.Controls.Add(this.pnlType);
            this.panel7.Location = new System.Drawing.Point(250, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1109, 537);
            this.panel7.TabIndex = 2;
            this.panel7.Tag = "Type here";
            // 
            // pnlVideoCall
            // 
            this.pnlVideoCall.Controls.Add(this.pnlRing);
            this.pnlVideoCall.Controls.Add(this.pictureBoxVideoIn);
            this.pnlVideoCall.Controls.Add(this.pictureBoxVideoOut);
            this.pnlVideoCall.Controls.Add(this.picManualStart);
            this.pnlVideoCall.Controls.Add(this.picStart);
            this.pnlVideoCall.Controls.Add(this.picStop);
            this.pnlVideoCall.Location = new System.Drawing.Point(605, 3);
            this.pnlVideoCall.Name = "pnlVideoCall";
            this.pnlVideoCall.Size = new System.Drawing.Size(490, 531);
            this.pnlVideoCall.TabIndex = 5;
            // 
            // pnlRing
            // 
            this.pnlRing.Controls.Add(this.picCutOut);
            this.pnlRing.Controls.Add(this.picCutIn);
            this.pnlRing.Controls.Add(this.picAnswerIn);
            this.pnlRing.Controls.Add(this.lblCallStatus);
            this.pnlRing.Controls.Add(this.pictureBoxGif);
            this.pnlRing.Location = new System.Drawing.Point(3, 3);
            this.pnlRing.Name = "pnlRing";
            this.pnlRing.Size = new System.Drawing.Size(484, 460);
            this.pnlRing.TabIndex = 25;
            // 
            // picCutOut
            // 
            this.picCutOut.Image = global::ChatApplication.Properties.Resources.Diclenr;
            this.picCutOut.Location = new System.Drawing.Point(360, 242);
            this.picCutOut.Name = "picCutOut";
            this.picCutOut.Size = new System.Drawing.Size(100, 33);
            this.picCutOut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCutOut.TabIndex = 8;
            this.picCutOut.TabStop = false;
            this.picCutOut.Click += new System.EventHandler(this.picCutOut_Click);
            // 
            // picCutIn
            // 
            this.picCutIn.Image = global::ChatApplication.Properties.Resources.Diclenr;
            this.picCutIn.Location = new System.Drawing.Point(360, 243);
            this.picCutIn.Name = "picCutIn";
            this.picCutIn.Size = new System.Drawing.Size(100, 33);
            this.picCutIn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCutIn.TabIndex = 7;
            this.picCutIn.TabStop = false;
            this.picCutIn.Click += new System.EventHandler(this.picCutIn_Click);
            // 
            // picAnswerIn
            // 
            this.picAnswerIn.Image = global::ChatApplication.Properties.Resources.answer__2_;
            this.picAnswerIn.Location = new System.Drawing.Point(41, 242);
            this.picAnswerIn.Name = "picAnswerIn";
            this.picAnswerIn.Size = new System.Drawing.Size(100, 33);
            this.picAnswerIn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picAnswerIn.TabIndex = 6;
            this.picAnswerIn.TabStop = false;
            this.picAnswerIn.Click += new System.EventHandler(this.picAnswerIn_Click);
            // 
            // lblCallStatus
            // 
            this.lblCallStatus.AutoSize = true;
            this.lblCallStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCallStatus.Location = new System.Drawing.Point(201, 242);
            this.lblCallStatus.Name = "lblCallStatus";
            this.lblCallStatus.Size = new System.Drawing.Size(81, 25);
            this.lblCallStatus.TabIndex = 2;
            this.lblCallStatus.Text = "Call...!";
            // 
            // pictureBoxGif
            // 
            this.pictureBoxGif.Image = global::ChatApplication.Properties.Resources.ring;
            this.pictureBoxGif.Location = new System.Drawing.Point(170, 136);
            this.pictureBoxGif.Name = "pictureBoxGif";
            this.pictureBoxGif.Size = new System.Drawing.Size(152, 93);
            this.pictureBoxGif.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxGif.TabIndex = 0;
            this.pictureBoxGif.TabStop = false;
            // 
            // pictureBoxVideoIn
            // 
            this.pictureBoxVideoIn.Image = global::ChatApplication.Properties.Resources.video;
            this.pictureBoxVideoIn.Location = new System.Drawing.Point(283, 302);
            this.pictureBoxVideoIn.Name = "pictureBoxVideoIn";
            this.pictureBoxVideoIn.Size = new System.Drawing.Size(198, 147);
            this.pictureBoxVideoIn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxVideoIn.TabIndex = 23;
            this.pictureBoxVideoIn.TabStop = false;
            // 
            // pictureBoxVideoOut
            // 
            this.pictureBoxVideoOut.Image = global::ChatApplication.Properties.Resources.video;
            this.pictureBoxVideoOut.Location = new System.Drawing.Point(3, 3);
            this.pictureBoxVideoOut.Name = "pictureBoxVideoOut";
            this.pictureBoxVideoOut.Size = new System.Drawing.Size(484, 460);
            this.pictureBoxVideoOut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxVideoOut.TabIndex = 24;
            this.pictureBoxVideoOut.TabStop = false;
            // 
            // picManualStart
            // 
            this.picManualStart.Image = global::ChatApplication.Properties.Resources.camon;
            this.picManualStart.Location = new System.Drawing.Point(143, 469);
            this.picManualStart.Name = "picManualStart";
            this.picManualStart.Size = new System.Drawing.Size(54, 54);
            this.picManualStart.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picManualStart.TabIndex = 31;
            this.picManualStart.TabStop = false;
            this.picManualStart.Click += new System.EventHandler(this.picManualStart_Click);
            // 
            // picStart
            // 
            this.picStart.Image = global::ChatApplication.Properties.Resources.calls;
            this.picStart.Location = new System.Drawing.Point(221, 469);
            this.picStart.Name = "picStart";
            this.picStart.Size = new System.Drawing.Size(54, 54);
            this.picStart.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picStart.TabIndex = 30;
            this.picStart.TabStop = false;
            this.picStart.Click += new System.EventHandler(this.picStart_Click);
            // 
            // picStop
            // 
            this.picStop.Image = global::ChatApplication.Properties.Resources.cutcall;
            this.picStop.Location = new System.Drawing.Point(301, 469);
            this.picStop.Name = "picStop";
            this.picStop.Size = new System.Drawing.Size(54, 54);
            this.picStop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picStop.TabIndex = 29;
            this.picStop.TabStop = false;
            this.picStop.Click += new System.EventHandler(this.picStop_Click);
            // 
            // pnlMsgWait
            // 
            this.pnlMsgWait.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pnlMsgWait.BackColor = System.Drawing.Color.White;
            this.pnlMsgWait.Controls.Add(this.pictureBox5);
            this.pnlMsgWait.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlMsgWait.Location = new System.Drawing.Point(0, 94);
            this.pnlMsgWait.Name = "pnlMsgWait";
            this.pnlMsgWait.Size = new System.Drawing.Size(598, 386);
            this.pnlMsgWait.TabIndex = 4;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackgroundImage = global::ChatApplication.Properties.Resources.lode;
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox5.Location = new System.Drawing.Point(216, 102);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(171, 168);
            this.pictureBox5.TabIndex = 0;
            this.pictureBox5.TabStop = false;
            // 
            // pnlSearchMessageContainer
            // 
            this.pnlSearchMessageContainer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pnlSearchMessageContainer.BackColor = System.Drawing.Color.White;
            this.pnlSearchMessageContainer.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlSearchMessageContainer.Location = new System.Drawing.Point(1, 94);
            this.pnlSearchMessageContainer.Name = "pnlSearchMessageContainer";
            this.pnlSearchMessageContainer.Size = new System.Drawing.Size(598, 386);
            this.pnlSearchMessageContainer.TabIndex = 3;
            // 
            // pnlMsg
            // 
            this.pnlMsg.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pnlMsg.BackColor = System.Drawing.Color.White;
            this.pnlMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlMsg.Location = new System.Drawing.Point(1, 94);
            this.pnlMsg.Name = "pnlMsg";
            this.pnlMsg.Size = new System.Drawing.Size(598, 386);
            this.pnlMsg.TabIndex = 2;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.panel8.Controls.Add(this.pnlVideoDial);
            this.panel8.Controls.Add(this.pnlConnecting);
            this.panel8.Controls.Add(this.lblSelectUserStatus);
            this.panel8.Controls.Add(this.lblSelectUserName);
            this.panel8.Controls.Add(this.shapeContainer4);
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(599, 91);
            this.panel8.TabIndex = 0;
            // 
            // pnlVideoDial
            // 
            this.pnlVideoDial.BackgroundImage = global::ChatApplication.Properties.Resources.telephone__1_;
            this.pnlVideoDial.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlVideoDial.Location = new System.Drawing.Point(523, 16);
            this.pnlVideoDial.Name = "pnlVideoDial";
            this.pnlVideoDial.Size = new System.Drawing.Size(63, 56);
            this.pnlVideoDial.TabIndex = 10;
            this.pnlVideoDial.Click += new System.EventHandler(this.pnlVideoDial_Click);
            // 
            // pnlConnecting
            // 
            this.pnlConnecting.Controls.Add(this.label2);
            this.pnlConnecting.Controls.Add(this.pictureBox3);
            this.pnlConnecting.Location = new System.Drawing.Point(324, 6);
            this.pnlConnecting.Name = "pnlConnecting";
            this.pnlConnecting.Size = new System.Drawing.Size(164, 81);
            this.pnlConnecting.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.SeaGreen;
            this.label2.Location = new System.Drawing.Point(47, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 14);
            this.label2.TabIndex = 1;
            this.label2.Text = "Connecting...";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::ChatApplication.Properties.Resources.load2;
            this.pictureBox3.Location = new System.Drawing.Point(46, 3);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(71, 50);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 0;
            this.pictureBox3.TabStop = false;
            // 
            // lblSelectUserStatus
            // 
            this.lblSelectUserStatus.AutoSize = true;
            this.lblSelectUserStatus.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSelectUserStatus.ForeColor = System.Drawing.Color.White;
            this.lblSelectUserStatus.Location = new System.Drawing.Point(117, 58);
            this.lblSelectUserStatus.Name = "lblSelectUserStatus";
            this.lblSelectUserStatus.Size = new System.Drawing.Size(46, 14);
            this.lblSelectUserStatus.TabIndex = 3;
            this.lblSelectUserStatus.Text = "ONLINE";
            // 
            // lblSelectUserName
            // 
            this.lblSelectUserName.AutoSize = true;
            this.lblSelectUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSelectUserName.ForeColor = System.Drawing.Color.White;
            this.lblSelectUserName.Location = new System.Drawing.Point(91, 26);
            this.lblSelectUserName.Name = "lblSelectUserName";
            this.lblSelectUserName.Size = new System.Drawing.Size(118, 20);
            this.lblSelectUserName.TabIndex = 1;
            this.lblSelectUserName.Text = "Ishara Madawa";
            // 
            // shapeContainer4
            // 
            this.shapeContainer4.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer4.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer4.Name = "shapeContainer4";
            this.shapeContainer4.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.ovlSelectUserStatus,
            this.picSelectedUser});
            this.shapeContainer4.Size = new System.Drawing.Size(599, 91);
            this.shapeContainer4.TabIndex = 5;
            this.shapeContainer4.TabStop = false;
            // 
            // ovlSelectUserStatus
            // 
            this.ovlSelectUserStatus.BackColor = System.Drawing.Color.Transparent;
            this.ovlSelectUserStatus.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ovlSelectUserStatus.BackgroundImage")));
            this.ovlSelectUserStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ovlSelectUserStatus.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.ovlSelectUserStatus.BorderWidth = 2;
            this.ovlSelectUserStatus.Cursor = System.Windows.Forms.Cursors.Default;
            this.ovlSelectUserStatus.Enabled = false;
            this.ovlSelectUserStatus.Location = new System.Drawing.Point(93, 54);
            this.ovlSelectUserStatus.Name = "ovlSelectUserStatus";
            this.ovlSelectUserStatus.SelectionColor = System.Drawing.Color.Transparent;
            this.ovlSelectUserStatus.Size = new System.Drawing.Size(20, 20);
            // 
            // picSelectedUser
            // 
            this.picSelectedUser.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("picSelectedUser.BackgroundImage")));
            this.picSelectedUser.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picSelectedUser.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.picSelectedUser.Location = new System.Drawing.Point(11, 10);
            this.picSelectedUser.Name = "picSelectedUser";
            this.picSelectedUser.SelectionColor = System.Drawing.Color.Transparent;
            this.picSelectedUser.Size = new System.Drawing.Size(68, 68);
            // 
            // pnlType
            // 
            this.pnlType.BackColor = System.Drawing.Color.Transparent;
            this.pnlType.Controls.Add(this.panel12);
            this.pnlType.Controls.Add(this.panel11);
            this.pnlType.Location = new System.Drawing.Point(0, 476);
            this.pnlType.Name = "pnlType";
            this.pnlType.Size = new System.Drawing.Size(599, 60);
            this.pnlType.TabIndex = 1;
            // 
            // panel12
            // 
            this.panel12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panel12.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.panel12.BackgroundImage = global::ChatApplication.Properties.Resources.send;
            this.panel12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel12.Location = new System.Drawing.Point(524, 14);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(46, 36);
            this.panel12.TabIndex = 2;
            this.panel12.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel12_MouseClick);
            this.panel12.MouseEnter += new System.EventHandler(this.panel12_MouseEnter);
            this.panel12.MouseLeave += new System.EventHandler(this.panel12_MouseLeave);
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.White;
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.txtMsgContent);
            this.panel11.Location = new System.Drawing.Point(39, 10);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(450, 42);
            this.panel11.TabIndex = 2;
            // 
            // txtMsgContent
            // 
            this.txtMsgContent.AcceptsTab = true;
            this.txtMsgContent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMsgContent.AutoCompleteCustomSource.AddRange(new string[] {
            "type here"});
            this.txtMsgContent.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.txtMsgContent.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMsgContent.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMsgContent.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMsgContent.Location = new System.Drawing.Point(21, 10);
            this.txtMsgContent.Multiline = true;
            this.txtMsgContent.Name = "txtMsgContent";
            this.txtMsgContent.Size = new System.Drawing.Size(410, 20);
            this.txtMsgContent.TabIndex = 2;
            this.txtMsgContent.TabStop = false;
            this.txtMsgContent.Tag = "Type Message Here";
            this.txtMsgContent.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMsgContent_KeyDown);
            // 
            // pnlAddContactContainer
            // 
            this.pnlAddContactContainer.Controls.Add(this.txtHideSearchContactId);
            this.pnlAddContactContainer.Controls.Add(this.pnlAddContactMsg);
            this.pnlAddContactContainer.Controls.Add(this.label6);
            this.pnlAddContactContainer.Controls.Add(this.pnlAddcontact);
            this.pnlAddContactContainer.Controls.Add(this.lblAddContact);
            this.pnlAddContactContainer.Controls.Add(this.shapeContainer1);
            this.pnlAddContactContainer.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlAddContactContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlAddContactContainer.Name = "pnlAddContactContainer";
            this.pnlAddContactContainer.Size = new System.Drawing.Size(598, 341);
            this.pnlAddContactContainer.TabIndex = 0;
            // 
            // txtHideSearchContactId
            // 
            this.txtHideSearchContactId.Location = new System.Drawing.Point(460, 63);
            this.txtHideSearchContactId.Name = "txtHideSearchContactId";
            this.txtHideSearchContactId.Size = new System.Drawing.Size(100, 20);
            this.txtHideSearchContactId.TabIndex = 5;
            this.txtHideSearchContactId.Visible = false;
            // 
            // pnlAddContactMsg
            // 
            this.pnlAddContactMsg.BackColor = System.Drawing.Color.MediumTurquoise;
            this.pnlAddContactMsg.Controls.Add(this.panel9);
            this.pnlAddContactMsg.Location = new System.Drawing.Point(137, 94);
            this.pnlAddContactMsg.Name = "pnlAddContactMsg";
            this.pnlAddContactMsg.Padding = new System.Windows.Forms.Padding(1);
            this.pnlAddContactMsg.Size = new System.Drawing.Size(333, 147);
            this.pnlAddContactMsg.TabIndex = 4;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.White;
            this.panel9.Controls.Add(this.pnlAddContactMsgCancel);
            this.panel9.Controls.Add(this.panel13);
            this.panel9.Controls.Add(this.txtAddContactMsg);
            this.panel9.Controls.Add(this.lblAddContactMsg);
            this.panel9.Controls.Add(this.shapeContainer5);
            this.panel9.Location = new System.Drawing.Point(4, 4);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(325, 139);
            this.panel9.TabIndex = 0;
            // 
            // pnlAddContactMsgCancel
            // 
            this.pnlAddContactMsgCancel.BackColor = System.Drawing.Color.MediumTurquoise;
            this.pnlAddContactMsgCancel.Controls.Add(this.lblAddContactMsgCancelBtn);
            this.pnlAddContactMsgCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlAddContactMsgCancel.Location = new System.Drawing.Point(108, 103);
            this.pnlAddContactMsgCancel.Name = "pnlAddContactMsgCancel";
            this.pnlAddContactMsgCancel.Size = new System.Drawing.Size(76, 24);
            this.pnlAddContactMsgCancel.TabIndex = 4;
            // 
            // lblAddContactMsgCancelBtn
            // 
            this.lblAddContactMsgCancelBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAddContactMsgCancelBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddContactMsgCancelBtn.ForeColor = System.Drawing.Color.White;
            this.lblAddContactMsgCancelBtn.Location = new System.Drawing.Point(0, 0);
            this.lblAddContactMsgCancelBtn.Name = "lblAddContactMsgCancelBtn";
            this.lblAddContactMsgCancelBtn.Size = new System.Drawing.Size(76, 24);
            this.lblAddContactMsgCancelBtn.TabIndex = 0;
            this.lblAddContactMsgCancelBtn.Text = "Cancel";
            this.lblAddContactMsgCancelBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblAddContactMsgCancelBtn.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lblAddContactMsgCancelBtn_MouseClick);
            this.lblAddContactMsgCancelBtn.MouseEnter += new System.EventHandler(this.label3_MouseEnter);
            this.lblAddContactMsgCancelBtn.MouseLeave += new System.EventHandler(this.label3_MouseLeave);
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.MediumTurquoise;
            this.panel13.Controls.Add(this.lblAddContactMsgSendButton);
            this.panel13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel13.Location = new System.Drawing.Point(218, 102);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(76, 24);
            this.panel13.TabIndex = 2;
            // 
            // lblAddContactMsgSendButton
            // 
            this.lblAddContactMsgSendButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAddContactMsgSendButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddContactMsgSendButton.ForeColor = System.Drawing.Color.White;
            this.lblAddContactMsgSendButton.Location = new System.Drawing.Point(0, 0);
            this.lblAddContactMsgSendButton.Name = "lblAddContactMsgSendButton";
            this.lblAddContactMsgSendButton.Size = new System.Drawing.Size(76, 24);
            this.lblAddContactMsgSendButton.TabIndex = 0;
            this.lblAddContactMsgSendButton.Text = "Send";
            this.lblAddContactMsgSendButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblAddContactMsgSendButton.Click += new System.EventHandler(this.lblAddContactMsgSendButton_Click);
            this.lblAddContactMsgSendButton.MouseEnter += new System.EventHandler(this.lblAddContactMsgSendButton_MouseEnter);
            this.lblAddContactMsgSendButton.MouseLeave += new System.EventHandler(this.lblAddContactMsgSendButton_MouseLeave);
            // 
            // txtAddContactMsg
            // 
            this.txtAddContactMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAddContactMsg.Location = new System.Drawing.Point(19, 35);
            this.txtAddContactMsg.Multiline = true;
            this.txtAddContactMsg.Name = "txtAddContactMsg";
            this.txtAddContactMsg.Size = new System.Drawing.Size(286, 51);
            this.txtAddContactMsg.TabIndex = 1;
            this.txtAddContactMsg.Text = "Hi Krishni Swandayani, I\'d like to add you as a contact.";
            // 
            // lblAddContactMsg
            // 
            this.lblAddContactMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddContactMsg.Location = new System.Drawing.Point(6, 9);
            this.lblAddContactMsg.Name = "lblAddContactMsg";
            this.lblAddContactMsg.Size = new System.Drawing.Size(299, 23);
            this.lblAddContactMsg.TabIndex = 0;
            this.lblAddContactMsg.Text = "Send Ishara Madawa a contact request";
            // 
            // shapeContainer5
            // 
            this.shapeContainer5.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer5.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer5.Name = "shapeContainer5";
            this.shapeContainer5.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.ovlAddContactMsgCancelBtnLeft,
            this.ovlAddContactMsgCancelBtnRight,
            this.ovlAddContactMsgRight,
            this.ovlAddContactMsgLeft});
            this.shapeContainer5.Size = new System.Drawing.Size(325, 139);
            this.shapeContainer5.TabIndex = 3;
            this.shapeContainer5.TabStop = false;
            // 
            // ovlAddContactMsgCancelBtnLeft
            // 
            this.ovlAddContactMsgCancelBtnLeft.BorderColor = System.Drawing.Color.MediumTurquoise;
            this.ovlAddContactMsgCancelBtnLeft.FillColor = System.Drawing.Color.MediumTurquoise;
            this.ovlAddContactMsgCancelBtnLeft.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.ovlAddContactMsgCancelBtnLeft.Location = new System.Drawing.Point(98, 103);
            this.ovlAddContactMsgCancelBtnLeft.Name = "ovlAddContactMsgCancelBtnLeft";
            this.ovlAddContactMsgCancelBtnLeft.Size = new System.Drawing.Size(23, 23);
            // 
            // ovlAddContactMsgCancelBtnRight
            // 
            this.ovlAddContactMsgCancelBtnRight.BorderColor = System.Drawing.Color.MediumTurquoise;
            this.ovlAddContactMsgCancelBtnRight.FillColor = System.Drawing.Color.MediumTurquoise;
            this.ovlAddContactMsgCancelBtnRight.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.ovlAddContactMsgCancelBtnRight.Location = new System.Drawing.Point(170, 103);
            this.ovlAddContactMsgCancelBtnRight.Name = "ovlAddContactMsgCancelBtnRight";
            this.ovlAddContactMsgCancelBtnRight.Size = new System.Drawing.Size(23, 23);
            // 
            // ovlAddContactMsgRight
            // 
            this.ovlAddContactMsgRight.BorderColor = System.Drawing.Color.MediumTurquoise;
            this.ovlAddContactMsgRight.FillColor = System.Drawing.Color.MediumTurquoise;
            this.ovlAddContactMsgRight.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.ovlAddContactMsgRight.Location = new System.Drawing.Point(280, 102);
            this.ovlAddContactMsgRight.Name = "ovlAddContactMsgRight";
            this.ovlAddContactMsgRight.Size = new System.Drawing.Size(23, 23);
            // 
            // ovlAddContactMsgLeft
            // 
            this.ovlAddContactMsgLeft.BorderColor = System.Drawing.Color.MediumTurquoise;
            this.ovlAddContactMsgLeft.FillColor = System.Drawing.Color.MediumTurquoise;
            this.ovlAddContactMsgLeft.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.ovlAddContactMsgLeft.Location = new System.Drawing.Point(207, 102);
            this.ovlAddContactMsgLeft.Name = "ovlAddContactMsgLeft";
            this.ovlAddContactMsgLeft.Size = new System.Drawing.Size(23, 23);
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.LightSeaGreen;
            this.label6.Location = new System.Drawing.Point(11, 108);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(575, 2);
            this.label6.TabIndex = 3;
            // 
            // pnlAddcontact
            // 
            this.pnlAddcontact.BackColor = System.Drawing.Color.LightSeaGreen;
            this.pnlAddcontact.Controls.Add(this.lblAddContactBtn);
            this.pnlAddcontact.Location = new System.Drawing.Point(227, 50);
            this.pnlAddcontact.Name = "pnlAddcontact";
            this.pnlAddcontact.Size = new System.Drawing.Size(147, 30);
            this.pnlAddcontact.TabIndex = 1;
            // 
            // lblAddContactBtn
            // 
            this.lblAddContactBtn.BackColor = System.Drawing.Color.LightSeaGreen;
            this.lblAddContactBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAddContactBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddContactBtn.ForeColor = System.Drawing.Color.White;
            this.lblAddContactBtn.Location = new System.Drawing.Point(0, 0);
            this.lblAddContactBtn.Name = "lblAddContactBtn";
            this.lblAddContactBtn.Size = new System.Drawing.Size(147, 30);
            this.lblAddContactBtn.TabIndex = 0;
            this.lblAddContactBtn.Text = "Add to Contacts";
            this.lblAddContactBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblAddContactBtn.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lblAddContactBtn_MouseClick);
            this.lblAddContactBtn.MouseEnter += new System.EventHandler(this.lblAddContactBtn_MouseEnter);
            this.lblAddContactBtn.MouseLeave += new System.EventHandler(this.lblAddContactBtn_MouseLeave);
            // 
            // lblAddContact
            // 
            this.lblAddContact.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddContact.ForeColor = System.Drawing.Color.DarkCyan;
            this.lblAddContact.Location = new System.Drawing.Point(11, 10);
            this.lblAddContact.Name = "lblAddContact";
            this.lblAddContact.Size = new System.Drawing.Size(575, 23);
            this.lblAddContact.TabIndex = 0;
            this.lblAddContact.Text = "Ishara Madawa not in your contacts";
            this.lblAddContact.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.ovlAddContactLeft,
            this.ovalAddContactRight});
            this.shapeContainer1.Size = new System.Drawing.Size(598, 341);
            this.shapeContainer1.TabIndex = 2;
            this.shapeContainer1.TabStop = false;
            // 
            // ovlAddContactLeft
            // 
            this.ovlAddContactLeft.BorderColor = System.Drawing.Color.LightSeaGreen;
            this.ovlAddContactLeft.FillColor = System.Drawing.Color.LightSeaGreen;
            this.ovlAddContactLeft.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.ovlAddContactLeft.Location = new System.Drawing.Point(214, 50);
            this.ovlAddContactLeft.Name = "ovlAddContactLeft";
            this.ovlAddContactLeft.Size = new System.Drawing.Size(29, 29);
            // 
            // ovalAddContactRight
            // 
            this.ovalAddContactRight.BorderColor = System.Drawing.Color.LightSeaGreen;
            this.ovalAddContactRight.FillColor = System.Drawing.Color.LightSeaGreen;
            this.ovalAddContactRight.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.ovalAddContactRight.Location = new System.Drawing.Point(357, 50);
            this.ovalAddContactRight.Name = "ovalAddContactRight";
            this.ovalAddContactRight.Size = new System.Drawing.Size(29, 29);
            // 
            // imgList
            // 
            this.imgList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgList.ImageStream")));
            this.imgList.TransparentColor = System.Drawing.Color.Transparent;
            this.imgList.Images.SetKeyName(0, "(458).JPG");
            this.imgList.Images.SetKeyName(1, "pic");
            this.imgList.Images.SetKeyName(2, "ajax-loader-arrows-blue-on-white.gif");
            // 
            // imgListStatus
            // 
            this.imgListStatus.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgListStatus.ImageStream")));
            this.imgListStatus.TransparentColor = System.Drawing.Color.Transparent;
            this.imgListStatus.Images.SetKeyName(0, "Online");
            this.imgListStatus.Images.SetKeyName(1, "Away");
            this.imgListStatus.Images.SetKeyName(2, "Invisible");
            this.imgListStatus.Images.SetKeyName(3, "Offline");
            // 
            // timerCheckConnection
            // 
            this.timerCheckConnection.Tick += new System.EventHandler(this.timerCheckConnection_Tick);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(849, 538);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel1);
            this.MaximumSize = new System.Drawing.Size(1375, 577);
            this.MinimumSize = new System.Drawing.Size(865, 577);
            this.Name = "Main";
            this.Text = "Via";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.pnlRecent.ResumeLayout(false);
            this.pnlRecentCount.ResumeLayout(false);
            this.pnlRecentCount.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.pnlInitialContact.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.pnlSearching.ResumeLayout(false);
            this.pnlSearching.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.pnlNoSearchResult.ResumeLayout(false);
            this.pnlNoSearchResult.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.pnlVideoCall.ResumeLayout(false);
            this.pnlRing.ResumeLayout(false);
            this.pnlRing.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCutOut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCutIn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAnswerIn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGif)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxVideoIn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxVideoOut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picManualStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picStop)).EndInit();
            this.pnlMsgWait.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.pnlConnecting.ResumeLayout(false);
            this.pnlConnecting.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.pnlType.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.pnlAddContactContainer.ResumeLayout(false);
            this.pnlAddContactContainer.PerformLayout();
            this.pnlAddContactMsg.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.pnlAddContactMsgCancel.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.pnlAddcontact.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lblUserStatus;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel pnlContact;
        private System.Windows.Forms.Panel pnlRecent;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label lblSelectUserStatus;
        private System.Windows.Forms.Label lblSelectUserName;
        private System.Windows.Forms.ImageList imgList;
        private System.Windows.Forms.Panel pnlType;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.TextBox txtMsgContent;
        private System.Windows.Forms.Panel pnlMsg;
        private System.Windows.Forms.ImageList imgListStatus;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer3;
        private Microsoft.VisualBasic.PowerPacks.OvalShape picBoxUserStatus;
        private Microsoft.VisualBasic.PowerPacks.OvalShape picBoxUser;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer4;
        private Microsoft.VisualBasic.PowerPacks.OvalShape ovlSelectUserStatus;
        private Microsoft.VisualBasic.PowerPacks.OvalShape picSelectedUser;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnlInitialContact;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label4;



        private System.Windows.Forms.FlowLayoutPanel floFriendList;
        private System.Windows.Forms.Panel pnlSearch;
        private System.Windows.Forms.FlowLayoutPanel flopnlSearch;
        private System.Windows.Forms.FlowLayoutPanel flopnlRecent;
        private System.Windows.Forms.Panel pnlConnecting;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Timer timerCheckConnection;
        private System.Windows.Forms.Label lblRecentCount;
        private Microsoft.VisualBasic.PowerPacks.OvalShape ovlRecentCount;
        private System.Windows.Forms.Panel pnlRecentCount;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private System.Windows.Forms.Panel pnlAddContactContainer;
        private System.Windows.Forms.Panel pnlAddContactMsg;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label lblAddContactMsgSendButton;
        private System.Windows.Forms.TextBox txtAddContactMsg;
        private System.Windows.Forms.Label lblAddContactMsg;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer5;
        private Microsoft.VisualBasic.PowerPacks.OvalShape ovlAddContactMsgRight;
        private Microsoft.VisualBasic.PowerPacks.OvalShape ovlAddContactMsgLeft;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel pnlAddcontact;
        private System.Windows.Forms.Label lblAddContactBtn;
        private System.Windows.Forms.Label lblAddContact;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.OvalShape ovlAddContactLeft;
        private Microsoft.VisualBasic.PowerPacks.OvalShape ovalAddContactRight;
        private System.Windows.Forms.Panel pnlAddContactMsgCancel;
        private System.Windows.Forms.Label lblAddContactMsgCancelBtn;
        private Microsoft.VisualBasic.PowerPacks.OvalShape ovlAddContactMsgCancelBtnLeft;
        private Microsoft.VisualBasic.PowerPacks.OvalShape ovlAddContactMsgCancelBtnRight;
        private System.Windows.Forms.Panel pnlNoSearchResult;
        private System.Windows.Forms.Label lblNoSearchResult;
        private System.Windows.Forms.Panel pnlSearching;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtHideSearchContactId;
        private System.Windows.Forms.Panel pnlSearchMessageContainer;
        private System.Windows.Forms.Panel pnlMsgWait;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Panel pnlVideoCall;
        private System.Windows.Forms.Panel pnlRing;
        private System.Windows.Forms.Label lblCallStatus;
        private System.Windows.Forms.PictureBox pictureBoxGif;
        private System.Windows.Forms.PictureBox pictureBoxVideoOut;
        private System.Windows.Forms.PictureBox pictureBoxVideoIn;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel pnlVideoDial;
        private System.Windows.Forms.PictureBox picAnswerIn;
        private System.Windows.Forms.PictureBox picCutIn;
        private System.Windows.Forms.PictureBox picCutOut;
        private System.Windows.Forms.PictureBox picManualStart;
        private System.Windows.Forms.PictureBox picStart;
        private System.Windows.Forms.PictureBox picStop;
        private System.Windows.Forms.Panel panel3;
        


    }
}

