﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic.PowerPacks;
using ChatApplication.ViaService;

namespace ChatApplication
{
    
    class Contact
    {
        private FlowLayoutPanel _flomsgContainer;
        private Panel _pnlContact = null;
        private OvalShape _ovlContactStatus = null;
        private OvalShape _ovlContactImage = null;
        private LoginStatus _loginStatus;
        private ViaService.ViaFriend _userDetails = null;
        public event ContactStatusChangeEventHandler ContactStatusChange;
        private bool _initialMsg = false;
        private Label _prevMsg  = new Label();
        private MessageArgs _prevMsgType = MessageArgs.SendingFail;
        private Int64 _prevMsgTime = 0;
        private int _notSeenMsgCount = 0;
        private Panel _pnlRecentContact = null;
        private OvalShape _ovlRecentContact = null;
        private Panel _pnlNotSeenCount;
        private Label _lblNotSeenCount;

        public FlowLayoutPanel FlomsgContainer
        {
            get
            {
                return this._flomsgContainer;
            }
        }

        public Panel PnlNotSeenCount
        {
            get
            {
                return this._pnlNotSeenCount;
            }
            set
            {
                this._pnlNotSeenCount = value;
            }
        }

        public Label LblNotSeenCount
        {
            get
            {
                return this._lblNotSeenCount;
            }
            set
            {
                this._lblNotSeenCount = value;
            }
        }

        public Label PrevMsg
        {
            get
            {
                return this._prevMsg;
            }
            set
            {
                this._prevMsg = value;
            }
        }

        public Panel PnlRecentContact
        {
            get
            {
                return this._pnlRecentContact;
            }
            set
            {
                this._pnlRecentContact = value;
            }
        }
        public OvalShape OvlRecentContact
        {
            get
            {
                return this._ovlRecentContact;
            }
            set
            {
                this._ovlRecentContact = value;
            }
        }

        public int NotSeenMsgCount
        {
            get
            {
                return this._notSeenMsgCount;
            }
            set
            {
                this._notSeenMsgCount = value;
            }
        }

        public MessageArgs PrevMsgType
        {
            get
            {
                return this._prevMsgType;
            }
            set
            {
                this._prevMsgType = value;
            }
        }

        public Int64 PrevMsgTime
        {
            get
            {
                return this._prevMsgTime;
            }
            set
            {
                this._prevMsgTime = value;
            }
        }
        public Panel PnlContact
        {
            get
            {
                return this._pnlContact;
            }
            set
            {
                this._pnlContact = value;
            }
        }

        public bool InitialMsg
        {
            get
            {
                return this._initialMsg;
            }
            set
            {
                this._initialMsg = value;
            }
        }

        public OvalShape OvlContactStatus
        {
            get
            {
                return this._ovlContactStatus;
            }
            set
            {
                this._ovlContactStatus = value;
            }
        }

        public OvalShape OvlContactImage
        {
            get
            {
                return this._ovlContactImage;
            }
            set
            {
                this._ovlContactImage = value;
            }
        }

        public LoginStatus LoginStatus
        {
            get
            {
                return this._loginStatus;
            }
            set
            {
                this._loginStatus = value;
                ContactStatusChangeEventArgs statusChangeEventData = new ContactStatusChangeEventArgs(value);
                OnContactStatusChange(statusChangeEventData);
            }
        }

        public ViaService.ViaFriend UserDetails 
        {
            get
            {
                return this._userDetails;
            }
        }

        protected virtual void OnContactStatusChange(ContactStatusChangeEventArgs e)
        {
            if (ContactStatusChange != null)
            {
                ContactStatusChange(this, e);
            }
        }

        public Contact(ViaService.ViaFriend userDetails)
        {
            UIControls ui = new UIControls();
            this._userDetails = userDetails;
            this._flomsgContainer = ui.GetMsgContainer(userDetails.ID);
        }


    }

    public class ContactStatusChangeEventArgs : EventArgs
    {
        private LoginStatus _loginStatus;

        public LoginStatus LoginStatus
        {
            get
            {
                return _loginStatus;
            }
        }

        public ContactStatusChangeEventArgs(LoginStatus status)
        {
            this._loginStatus = status;
        }
    }

    public delegate void ContactStatusChangeEventHandler(object sender, ContactStatusChangeEventArgs e);
}
