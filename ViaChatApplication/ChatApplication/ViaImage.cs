﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatApplication
{
    public class ViaImage
    {
        private ImageFormat imageFormat;
        public byte[] ImageToByteArray(Image image)
        {
            if (image.RawFormat.Equals(ImageFormat.Jpeg))
                imageFormat = ImageFormat.Jpeg;
            if (image.RawFormat.Equals(ImageFormat.Gif))
                imageFormat = ImageFormat.Gif;
            if (image.RawFormat.Equals(ImageFormat.Png))
                imageFormat = ImageFormat.Png;

            MemoryStream ms = new MemoryStream();
            image.Save(ms, imageFormat);
            return ms.ToArray();
        }

        public static Image ByteArrayToImage(byte[] byteArray) 
        {
            MemoryStream ms = new MemoryStream(byteArray);
            return Image.FromStream(ms);
        }

        public Image ResizeImage(Image image,Size size)
        {
            return (Image)(new Bitmap(image, size));
        }
    }
}
