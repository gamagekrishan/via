$(document).ready(function() {
	$(document).scroll(function() {
		$('.navbar-fixed-bottom').hide();
	});
	$('#freeuser').on('click', function(e) {
		loadFormModal('#modal-signup');
	});
	$('#silveruser').on('click', function(e) {
		updateserver('silver');
		$('#paidusage').modal();
	});
	$('#golduser').on('click', function(e) {
		updateserver('gold');
		$('#paidusage').modal();
	});
	$('#platinumuser').on('click', function(e) {
		updateserver('platinum');
		$('#paidusage').modal();
	});
	$.validator.addMethod(
		    "regex",
		    function(value, element, regexp) {
		        var check = false;
		        var re = new RegExp(regexp);
		        return this.optional(element) || re.test(value);
		    },
		    "Only characters are allowed"
		);
	$(function () {

        $(document).keydown(function (e) {

            return (e.which || e.keyCode) != 116;

        });

    });
});

function updateserver(plan) {
	var inputResponse = {"id" : plan };
	$.ajax({
		type : 'POST',
		url : '/pricing/stats',
		cache : false,
		contentType : 'application/json; charset=UTF-8',
		processData : false,
		   dataType : 'json',
			   data : JSON.stringify(inputResponse),
		 converters : {"text json" : jQuery.parseJSON},
			success : function(response) {
					}
	});
}

var loadFormModal = function(theModal)
{
	$('#the-form').load('/module/form.html > ' + theModal, function () {
		$(theModal).modal('show');
		onFormModalHide(theModal);
		onFormSubmit($(theModal + ' form'), $(theModal));
		if (theModal == '#modal-login')
			{
			func_password_forget_and_remember();
			}
	});
}

var onFormModalHide = function (theModal) {
	$(theModal).on('hidden.bs.modal', function () {
		//first, unregister the handlers/listeners pertaining to the form
		
		// vacate the form from the page
		$(this).parent().empty();
	});
}

var onFormSubmit = function(theForm, theModalHostingTheForm)
{
	// theModalHostingTheForm.modal('hide');
	if (theForm.attr('id') == 'form-signup')
		theForm.validate(formSignupValidation);
	else if (theForm.attr('id') == 'form-login')
		theForm.validate(formLoginValidation);
	
	
}

var func_password_forget_and_remember = function () {
	
	$('#the-form #modal-login #memory-password').on('click', function (e) {
		e.preventDefault();
		
		var msg = $('#memory-password > span');
		var pwd = $('#inputLoginPassword');
		var labelPwd = $('#l-password');
		var form = $(this).closest('form');

		if (msg.text() == 'I forgot my password.')
		{
			pwd.prop('required', false);
			pwd.attr('type', 'hidden');
			labelPwd.hide();
			pwd.val('');
			$('#inputPasswordReset').val('true');
			$('#btn-login').text('reset password');
			msg.text('I remember it now!');
			form.attr('action', '/resetPassword');
		}
		else if (msg.text() == 'I remember it now!')
		{
			pwd.prop('required', true);
			pwd.attr('type', 'password');
			labelPwd.show();
			$('#inputPasswordReset').val('false');
			$('#btn-login').text('sign in');
			msg.text("I forgot my password.");
			form.attr('action', '/login');
		}
	});
}





var formSignupValidation = {
	rules:
	{
		name: {
			required: true,
			minlength: 2,
			maxlength: 59,
			regex:/^[A-Za-z\s.]+$/
		},
		signupEmail: {
			required: true,
			email: true
		},
		password: {
			required: true,
			minlength: 6
		},
		organization: {
			maxlength: 59
		},
		"phone-number": {
			required: true,
			digits: true,
			minlength: 10,
			maxlength: 13
		}
	}
};

var formLoginValidation = {
	rules:
	{
		email: {
			required: true,
			email: true
		},
		password: {
			required: true
		}
		
	}
};

$('.formPopper').on('click', function (e) {
	e.preventDefault();
	loadFormModal('#' + $(this).attr('data-target'));
});
