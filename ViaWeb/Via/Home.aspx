﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Via.Home" MasterPageFile="~/Index.Master" %>


    <asp:Content ID="ContentHome" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <!-- row 1 -->
        <div class =" container">
        <div class="row" style="background-image:url('../Images/home.jpg');height:400px; width:100%; background-repeat:no-repeat;background-size:cover; margin-top:-10px; text-align:center;">
            <h1 style="font-family:Impact; color:white; margin-top:150px;">Connect with Via</h1>
            <h3 style="font-family:Impact; color:white">It's Free and Easy</h3>
        </div>
       
        <!-- row 2 -->
    <div class="row">
        <div style="margin-top:20px;">
            <div class="col-lg-3" style="margin-left:250px; text-align:center">
                <div><h2 style="font-family:'Baskerville Old Face'">Connect to Via</h2></div>
        <asp:Button ID="Button1" runat="server" Text="Sign In" CssClass="btn btn-info"  OnClick="Button1_Click" />
                </div>
            <div class="col-lg-3" style="text-align:center">
                <div><h2 style="font-family:'Baskerville Old Face'">Join with us</h2></div>
        <asp:Button ID="Button2" runat="server" Text="Sign Up Now" CssClass="btn btn-danger"  OnClick="Button2_Click" />
                </div>
        </div>
    </div>
  </div>
</asp:Content>