﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace Via
{
    public partial class Profile : System.Web.UI.Page
    {
        string strConnString = ConfigurationManager.ConnectionStrings["RegistrationConnectionString"].ConnectionString;
        string str;
        SqlDataAdapter sqlda;
        DataSet ds;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindgrid();
            }
 
        }

        private void bindgrid()
        {
            //string UserName = (string)Session["New"];
            //SqlConnection con = new SqlConnection(strConnString);
            //con.Open();
            //str = "SELECT * FROM Userdata WHERE F_Name='" + UserName + "'";
            //sqlda = new SqlDataAdapter(str, con);
            //ds = new DataSet();
            //sqlda.Fill(ds, UserName);
            //lblFName.Text = ds.Tables["Userdata"].Rows[0]["F_Name"].ToString();
            //lblLName.Text = ds.Tables["Userdata"].Rows[0]["L_Name"].ToString();
            //lblEmail.Text = ds.Tables["Userdata"].Rows[0]["Email"].ToString();
            //lblAddress.Text = ds.Tables["Userdata"].Rows[0]["Address"].ToString();
            //lblDOB.Text = ds.Tables["Userdata"].Rows[0]["DOB"].ToString();
            //lblTel.Text = ds.Tables["Userdata"].Rows[0]["Tel"].ToString();
            //lblGender.Text = ds.Tables["Userdata"].Rows[0]["Gender"].ToString();

            //con.Close();
            ViaWebService.ViaUser newUser = new ViaWebService.ViaUser();
            newUser = (ViaWebService.ViaUser)Session["New"];
            //profilePic.Style= ViaImage.ByteArrayToImage(newUser.Image);
            lblFName.Text = newUser.FirstName.ToString();
            lblLName.Text = newUser.LastName.ToString();
            lblEmail.Text = newUser.Emial.ToString();
            lblAddress.Text = "Colombo";
            lblDOB.Text = "10/11/1992";
            lblTel.Text = "0712197222";
            lblGender.Text = "Male";

            //Old Method
            //string UserName = (string)Session["New"];
            //SqlConnection con1 = new SqlConnection(strConnString);
            //DataTable dt = new DataTable();
            //con1.Open();
            //SqlDataReader myReader = null;
            //SqlCommand myCommand = new SqlCommand("SELECT * FROM Userdata WHERE F_Name='" + UserName + "'",con1);

            //myReader = myCommand.ExecuteReader();

            //while (myReader.Read())
            //{
            //    lblFName.Text = (myReader["F_Name"].ToString());
            //    lblLName.Text = (myReader["L_Name"].ToString());
            //    lblEmail.Text = (myReader["Email"].ToString());
            //    lblAddress.Text = (myReader["Address"].ToString());
            //    lblDOB.Text = (myReader["DOB"].ToString());
            //    lblTel.Text = (myReader["Tel"].ToString());
            //    lblGender.Text = (myReader["Gender"].ToString());
                
            //}
            //con1.Close();


        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Session["New"] = null;
            Response.Redirect("Login.aspx");
        }

        protected void Button1_Click1(object sender, EventArgs e)
        {
            Session["New"] = null;
            Response.Redirect("Login.aspx");
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}