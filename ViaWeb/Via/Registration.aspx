﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Registration.aspx.cs" Inherits="Via.Registration" MasterPageFile="~/Index.Master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    
    <div class="container">
        <div class="row">
            <h1>Sign Up</h1>
        </div>
        <div class="row">
            
             <div class="col-md-3 table-bordered" style="padding:10px; border-radius:10px;">
                 <h3>Choose Profile Pitcher</h3>
                 <div id="dvPreview" runat="server" style="margin-left:30px; margin-bottom:10px;">
                 </div>
                 <asp:FileUpload id="fileupload" runat="server" />
                 <asp:Label ID="lblMessage" runat="server" Text="" Font-Names ="Arial"></asp:Label>



            </div>
    <div class="col-md-7 col-md-push-1 table-bordered" style="border-radius:10px;">
        <h3>Fill Your Information</h3>
        <table class="auto-style1">
            <tr>
                <td class="auto-style3">First Name</td>
                <td class="auto-style10">
                    <asp:TextBox ID="txtFName" CssClass="form-control"  runat="server" Width="399px"></asp:TextBox>
                </td>
                <td class="auto-style18">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="TxtFName" ErrorMessage="Enter First Name" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">Last Name</td>
                <td class="auto-style10">
                    <asp:TextBox ID="txtLName" CssClass="form-control"  runat="server" Width="399px"></asp:TextBox>
                </td>
                <td class="auto-style18">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TxtFName" ErrorMessage="Enter First Name" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style5">Email</td>
                <td class="auto-style12">
                    <asp:TextBox ID="TxtEmail" runat="server" CssClass="form-control"  Width="398px"></asp:TextBox>
                </td>
                <td class="auto-style20">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TxtEmail" ErrorMessage="Email Requierd" ForeColor="Red"></asp:RequiredFieldValidator>
                    <br />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TxtEmail" ErrorMessage="Enter valid Email" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style26">Location</td>
                <td class="auto-style13">
                    <asp:TextBox ID="TxtAdd" CssClass="form-control"  runat="server" Width="394px"></asp:TextBox>
                </td>
                <td class="auto-style21">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="TxtAdd" ErrorMessage="Enter Address" ForeColor="Red"></asp:RequiredFieldValidator>
                    <br />
                </td>
            </tr>
            <tr>
                <td class="auto-style7">Date Of Birth</td>
                <td class="auto-style14">
                    <asp:TextBox ID="TxtDOB" CssClass="form-control"  runat="server" Width="394px"></asp:TextBox>
                </td>
                <td class="auto-style22">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="TxtDOB" ErrorMessage="Enter Date Of Birth" ForeColor="Red"></asp:RequiredFieldValidator>
                    <br />
                </td>
            </tr>
            
            <tr>
                <td class="auto-style9">Password</td>
                <td class="auto-style15">
                    <asp:TextBox ID="TxtPass" runat="server" CssClass="form-control" TextMode="Password" Width="395px"></asp:TextBox>
                </td>
                <td class="auto-style23">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TxtPass" ErrorMessage="Password Requaeird" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style16">Conferm Password</td>
                <td class="auto-style17">
                    <asp:TextBox ID="TxtConPass" CssClass="form-control"  runat="server" TextMode="Password" Width="394px"></asp:TextBox>
                </td>
                <td class="auto-style24">
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="TxtPass" ControlToValidate="TxtConPass" ErrorMessage="Both Password Mustbe Same" ForeColor="Red"></asp:CompareValidator>
                    <br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TxtConPass" ErrorMessage="Confirm Password Requieard" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
            </tr>
            <tr>
                <td class="auto-style26">Tel</td>
                <td class="auto-style13">
                    <asp:TextBox ID="TxtTel" CssClass="form-control"  runat="server" Width="395px"></asp:TextBox>
                </td>
                <td class="auto-style21">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="TxtTel" ErrorMessage="Enter Telephone Number" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <%--<tr>
                <td class="auto-style5">Gender</td>
                <td class="auto-style12">
                    <asp:DropDownList ID="DropDownListGender" CssClass="form-control" runat="server" Width="420px">
                        <asp:ListItem>Select Gender</asp:ListItem>
                        <asp:ListItem>Male</asp:ListItem>
                        <asp:ListItem>Female</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="auto-style20">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="DropDownListGender" ErrorMessage="Select Gender" ForeColor="Red" InitialValue="Select Country"></asp:RequiredFieldValidator>
                </td>
            </tr>--%>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td class="auto-style8">&nbsp;</td>
                <td class="auto-style25">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td class="auto-style8">
                    <asp:Button id="Button1" ValidationGroup="Details" CssClass="btn btn-success" runat="server" OnClick="Button1_Click" Text="Submit" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input id="Reset1" class="btn btn-warning" type="reset" value="reset" /></td>
                <td class="auto-style25">&nbsp;</td>
            </tr>
        </table>
        </div>
           
    </div>
    </div>
 

    </asp:Content>
<asp:Content ID="Content3" runat="server" contentplaceholderid="head">
    <style type="text/css">
        .auto-style1 {
            height: 51px;
        }
        .auto-style2 {
            height: 55px;
            width: 224px;
        }
        .auto-style3 {
            height: 66px;
            width: 224px;
        }
        .auto-style4 {
            height: 64px;
            width: 224px;
        }
        .auto-style5 {
            height: 63px;
            width: 224px;
        }
        .auto-style7 {
            height: 65px;
            width: 224px;
        }
        .auto-style8 {
            height: 58px;
            width: 572px;
        }
        .auto-style9 {
            height: 89px;
            width: 224px;
        }
        .auto-style10 {
            height: 66px;
            width: 572px;
        }
        .auto-style11 {
            height: 64px;
            width: 572px;
        }
        .auto-style12 {
            height: 63px;
            width: 572px;
        }
        .auto-style13 {
            height: 67px;
            width: 572px;
        }
        .auto-style14 {
            height: 65px;
            width: 572px;
        }
        .auto-style15 {
            height: 89px;
            width: 572px;
        }
        .auto-style16 {
            height: 62px;
            width: 224px;
        }
        .auto-style17 {
            height: 62px;
            width: 572px;
        }
        .auto-style18 {
            height: 66px;
            width: 373px;
        }
        .auto-style19 {
            height: 64px;
            width: 373px;
        }
        .auto-style20 {
            height: 63px;
            width: 373px;
        }
        .auto-style21 {
            height: 67px;
            width: 373px;
        }
        .auto-style22 {
            height: 65px;
            width: 373px;
        }
        .auto-style23 {
            height: 89px;
            width: 373px;
        }
        .auto-style24 {
            height: 62px;
            width: 373px;
        }
        .auto-style25 {
            width: 373px;
        }
        .auto-style26 {
            height: 67px;
            width: 224px;
        }
    </style>
</asp:Content>
